# Recipe App API Proxy   

NGINX proxy app for our recipe app API

# Usage

## Environment variable 

* 'LISTEN_PORT' - Port to listen on (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app')
* 'APP_PORT' - port og the app to forward request to (default: '9000')S